#include "tools.h"
#include <errno.h>
#include <termios.h>
#include <unistd.h>

int initPachube(){
  int sock = createTcpSocket();
  char *ip;
//   resolveHost("HOST",ip);
  fprintf(stderr, "IP is %s\n", ip); 
  struct sockaddr_in *remote = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in *));
  remote->sin_family = AF_INET;
  int tmpres = inet_pton(AF_INET, ip, (void *)(&(remote->sin_addr.s_addr)));
  if( tmpres < 0){
    perror("Can't set remote->sin_addr.s_addr");
    return 1;
  }else if(tmpres == 0){
    fprintf(stderr, "%s is not a valid IP address\n", ip);
    return 1;
  }
  remote->sin_port = htons(PORT);
 
  if(connect(sock, (struct sockaddr *)remote, sizeof(struct sockaddr)) < 0){
    perror("Could not connect");
    return 1;
  }
//   get = build_get_query(host, page);
//   fprintf(stderr, "Query is:\n<<START>>\n%s<<END>>\n", get);
  return sock;
 
}

int sendTcpData(char * data, char * host,struct sockaddr_in * sockadd,int hostip){
   char ip[16];
  int  sock   =createTcpSocket();
  printf("sendTcpData:[%d]\n",sock);
  printf("adas\n");
  //Send the query to the server
  int sent = 0;
  int tmpres=0;
 char buf[BUFSIZ+1];
//   if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0){
//     perror("Can't create TCP socket");
//     return -1;
//   }
  if(hostip){
    if(resolveHost(host,ip)){
      return 1;
    }
  }else{
    strcpy(ip,host);
  }
    
  sockadd->sin_family = AF_INET;
  tmpres = inet_pton(AF_INET, ip, (void *)(&((sockadd->sin_addr.s_addr))));
  printf("adas\n");
  if( tmpres < 0){
    perror("Can't set remote->sin_addr.s_addr");
    return(1);
  }else if(tmpres == 0)  {
    fprintf(stderr, "%s is not a valid IP address\n", ip);
    return(1);
  }else
    printf("ip ok:%s\n",ip);
  sockadd->sin_port = htons(PORT);
 
  if(connect(sock, (struct sockaddr *)sockadd, sizeof(struct sockaddr)) < 0){
    perror("Could not connect");
    return(1);
  }
  

  while(sent < strlen(data)){
    printf("sent:[%d]of[%d]\n",sent,(int)strlen(data));
    
    tmpres = send(sock, data+sent, strlen(data)-sent, 0);
    if(tmpres == -1){
      perror("Can't send query");
      exit(1);
    }
    sent += tmpres;
  }
  printf("Msg sent.\n");
  //now it is time to receive the page
  memset(buf, 0, sizeof(buf));
  int htmlstart = 0;
  char * htmlcontent;
  while((tmpres = recv(sock, buf, BUFSIZ, 0)) > 0){
    if(htmlstart == 0){
      /* Under certain conditions this will not work.
      * If the \r\n\r\n part is splitted into two messages
      * it will fail to detect the beginning of HTML content
      */
      htmlcontent = strstr(buf, "\r\n\r\n");
      if(htmlcontent != NULL){
        htmlstart = 1;
        htmlcontent += 4;
      }
    }else{
      htmlcontent = buf;
    }
    if(htmlstart){
      fprintf(stdout, htmlcontent);
    }
 
    memset(buf, 0, tmpres);
  }
  if(tmpres < 0){
    perror("Error receiving data");
  }
  free(data);
  close(sock);
  return 0;
}

int senUdpdData( int fd,char *buffer, unsigned int len){
  int ret=0;

  printf("\nSEND_DATA:\n%s\n",buffer);

  while (len){	
    ret= send(fd, buffer, len, 0);
    if (ret == -1){
      perror("Err! send");
      close(fd);
      return 1;
    }
    len-=ret;
    buffer+=ret;
  }
  return 0;
}

int createTcpSocket(){
  int sock;
  if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0){
    perror("Can't create TCP socket");
    return -1;
  }
  
  return sock;
}
/*
int connectTcpSocket(int sock,char * ip){
  if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0){
    perror("Can't create TCP socket");
    return -1;
  }
  fprintf(stderr, "IP is %s\n", ip); 
//   struct sockaddr_in * remote = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in *));
  if(resolveHost("api.pachube.com",ip))
    return 1;
//   g_sockadd.sin_family = AF_INET;
  int tmpres = inet_pton(AF_INET, ip, (void *)(&(g_sockadd.sin_addr.s_addr)));
  if( tmpres < 0){
    perror("Can't set remote->sin_addr.s_addr");
    return(1);
  }else if(tmpres == 0)  {
    fprintf(stderr, "%s is not a valid IP address\n", ip);
    return(1);
  }
  g_sockadd.sin_port = htons(PORT);
 
  if(connect(sock, (struct sockaddr *)&g_sockadd, sizeof(struct sockaddr)) < 0){
    perror("Could not connect");
    return(1);
  }
  return 0;
}
*/
char *buildPachubeQuery(char *feed, char *key, char * data){
  char *query;
//   char *getpage = page;
//   char *tpl = "GET /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n";
  char *tpl = "PUT /v2/feeds/%s.csv HTTP/1.1\r\nHost: api.pachube.com\r\nX-PachubeApiKey: %s\r\nUser-Agent: asdf\r\nContent-Length: 18\r\nContent-Type: text/csv\r\nConnection: close\r\n\r\n%s";
//   char *tpl = "PUT /v2/feeds/83209.csv HTTP/1.1\r\nHost: api.pachube.com\r\nX-PachubeApiKey: jbiVfSss93htCUiBwyMZUr8PFaSSAKxaaU9uZGZzcUFsST0g\r\nUser-Agent: asdf\r\nContent-Length: 18\r\nContent-Type: text/csv\r\nConnection: close\r\n\r\nsensor1,10\ntemp,35";
//   if(getpage[0] == '/'){
//     getpage = getpage + 1;
//     fprintf(stderr,"Removing leading \"/\", converting %s to %s\n", page, getpage);
//   }
  // -5 is to consider the %s %s %s in tpl and the ending \0
  query = (char *)malloc(strlen(feed)+strlen(key)+strlen(data)+strlen(tpl)-5);
  sprintf(query,tpl,feed,key,data);
  return query;
}

int printDS(struct sockaddr_in ds) { 
 
  printf("ds = %d, %s, %d\n", ds.sin_family, 
    inet_ntoa(ds.sin_addr), ntohs(ds.sin_port)); 
  return 0;
} 

int readSerial(int fd){
//   char buf[BUFSIZ];
  int i, n;
  char buf[2];
    /*
      * Read the data.
      */
    if(g_buf_new==1){
      g_buf_new=0;
      g_buf_pos=0;
      
      memset(g_buf_serial,0,MSG_MAX); 
    }
    n = read(fd, buf, 1);
    buf[1]='\0';
    printf("%s\n",buf);
    
    if((buf[0]== '\n')||(buf[0]== '\r')){
       g_buf_serial[g_buf_pos]='\0';
       g_buf_new=1;
       strncpy(g_msg,g_buf_serial,g_buf_pos);
       printf("%s\n",g_msg);
      return 0;
    }else{
      g_buf_serial[g_buf_pos] =buf[0];
      g_buf_pos++;
       printf("%s\n",g_buf_serial);
      return 1;
    }
}

int readUdpSocket(int fd){
  printf("readUdpSocket\n");
  int n ;
  struct sockaddr_in rsock;
  int len = sizeof(rsock); 
  rsock.sin_family = AF_INET; 
 
  if ((n = recvfrom(fd, g_msg,sizeof(g_msg),0,(struct sockaddr *) &rsock, &len)) < 0){ 
    perror("fallo al recibir Mensaje") ; 
    return 1;
  }else {
//     g_msg[n]='\0';
    printf("Mensaje Recibido:(%s)longitud = %d \n", g_msg, n); 
    return 0;
  }
}

int set_interface_attribs (int fd, int speed, int parity){
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                perror ("error %d from tcgetattr");
                return -1;
        }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // ignore break signal
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 10;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0){
                perror ("error %d from tcsetattr");
                return -1;
        }
        return 0;
}

void set_blocking (int fd, int should_block){
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0){
                perror ("error %d from tggetattr");
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                perror ("error %d setting term attributes");
}


// ...
// char *portname = "/dev/ttyUSB1"
//  ...
// int fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
// if (fd < 0)
// {
//         error_message ("error %d opening %s: %s", errno, portname, strerror (errno));
//         return;
// }

// set_interface_attribs (fd, B115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
// set_blocking (fd, 0);                // set no blocking
// 
// write (fd, "hello!\n", 7);           // send 7 character greeting
// char buf [100];
// int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read

/*The values for speed are B115200, B230400, B9600, B19200, B38400, B57600, B1200, B2400, B4800, etc. The values for parity are 0 (meaning no parity), PARENB|PARODD (enable parity and use odd), PARENB (enable parity and use even), PARENB|PARODD|CMSPAR (mark parity), and PARENB|CMSPAR (space parity).

"Blocking" sets whether a read() on the port waits for the specified number of characters to arrive. Setting no blocking means that a read() returns however many characters are available without waiting for more, up to the buffer limit.*/
int openSerial(char *files){
  printf("open:%s\n",files);
  int fd=0;
  if ((fd = open(files, O_RDWR | O_NOCTTY | O_SYNC)) < 0) {
    perror(files);
    exit(1);
  }
  set_interface_attribs (fd, B9600, 0);  // set speed to 115,200 bps, 8n1 (no parity)
  set_blocking (fd, 1);                // set no blocking


  /*
  * Make sure it's a tty.
  */
  if (!isatty(fd)) {
    fprintf(stderr, "All files must be tty devices.\n");
    exit(1);
  }
  printf("Open Ok\n");
  return fd;
}

int openUdpSocket( int puerto ){
  int s;
  struct sockaddr_in sock;
  if ((s = socket(AF_INET, SOCK_DGRAM, 0)) < 0) { 
    perror("fallo al abrir el socket"); 
    return; 
  } 
  sock.sin_family = AF_INET; 
  sock.sin_port = htons(puerto); 
  sock.sin_addr.s_addr = htonl(INADDR_ANY); 

  if (bind(s, (struct sockaddr *) &sock, TAM_SOCKET)!= 0) { 
    perror("fallo al enlazar el socket\n"); 
    close(s); 
    return; 
  } 
  return s;
  
} 

int resolveHost(char * host, char *ip){
  printf("resolveHost\n");
  struct hostent *he;

  if ((he=gethostbyname(host)) == NULL){
    perror("Err!! gethostbyname");
    return 1;
  }

  if(inet_ntop(AF_INET, (void *)he->h_addr_list[0], ip, IP_MAX) == NULL){ //16 = xxx.xxx.xxx.xxx + '\0' (ip)
    perror("Err!! resolving host");
    return 1;
  }
  printf("host: %s, ip: %s\n",host, ip);
  return 0;
}

// int resolveHost(char *host,char *ip){
//   struct hostent *hent;
// //   int iplen = 15; //XXX.XXX.XXX.XXX
// //   char *ip = (char *)malloc(iplen+1);
// //   memset(ip, 0, iplen+1);
//   if((hent = gethostbyname(host)) == NULL)  {
//     herror("Can't get IP");
//     return(1);
//   }
//   if(inet_ntop(AF_INET, (void *)hent->h_addr_list[0], ip, sizeof(ip)) == NULL)  {
//     perror("Can't resolve host");
//     return(1);
//   }
//   return 0;
// }

