#include <uci.h>
#include <stdio.h>
#include <string.h>
#include "config.h"
#include <stdlib.h>



static enum {
	CLI_FLAG_MERGE =    (1 << 0),
	CLI_FLAG_QUIET =    (1 << 1),
	CLI_FLAG_NOCOMMIT = (1 << 2),
	CLI_FLAG_BATCH =    (1 << 3),
	CLI_FLAG_SHOW_EXT = (1 << 4),
	CLI_FLAG_NOPLUGINS= (1 << 5),
} flags;

static struct uci_context *ctx;
static const char *appname;
char configFile[15];
char * opt[]={"id","gate","ip","name","feed","key","nparam"};

void initConfig(char * confFile){
  strcpy(configFile,confFile);
  ctx = uci_alloc_context();
//   uci_set_confdir(ctx,".");
}

int get(char *data, char *dst){
  struct uci_element *e;
  struct uci_ptr ptr;
  char strData[100];
  memset(strData,0,100);
  if(data==NULL){
    perror("Input data empty, please verify the input string\n");
    return 1;
  }
  strcat(strData,configFile);
  strcat(strData,".");
  strcat(strData,data);
//   printf("get:[%s]\n",strData);
  // 	strcpy(strData,(char *)(data.c_str()));
  if(ctx == NULL){
    ctx = uci_alloc_context();
  }
  if (uci_lookup_ptr(ctx, &ptr, strData, true) != UCI_OK) {
    fprintf(stderr,"Could not get [%s]\n",strData);
    cli_perror();
    return 1;
  }
  if (ptr.value){
    fprintf(stderr,"Could not get [%s]\n",strData);
    return 1;
  }
  e = ptr.last;
  if(!(ptr.flags & UCI_LOOKUP_COMPLETE)) {
    ctx->err = UCI_ERR_NOTFOUND;
    fprintf(stderr,"Could not get [%s]\n",strData);
    cli_perror();
    return 1;
  }
  switch(e->type) {
    case UCI_TYPE_SECTION:
      strcpy(dst,ptr.s->type);
      break;
    case UCI_TYPE_OPTION:
      if(ptr.o->type == UCI_TYPE_STRING){
	strcpy(dst,ptr.o->v.string);
      } else {
	perror("Value not Found\n");
	return 1;
      }
      break;
    default:
      break;
  }
  return 0;
};

// int readConfig(char ** p,int item, int subitem,int size ){
int readConfig(void){
  readGateways();
  readModules();
}

int readGateways(void){
  printf("readGateways\n");
  char tmp[30];
  char command[30];
   if(get("global.ngateway",tmp)){
    perror("reading config problems:\n");
    return 1;
  }
  if(tmp==NULL){
    perror("reading global.nmodules:\n");
    return 1;
  }
  nGates=atoi(tmp);
  printf("INFO: Reads %d gateways\n",nGates);
  int i,ret;
  vGates=(sGate **) malloc (nGates*sizeof(char*));
  for(i=0;i<nGates;i++){
    
    vGates[i]=(sGate *) malloc (sizeof(sGate));
    sprintf(command,"gateway%d.name",i);
    ret=get(command,tmp);
    if((ret)||(tmp==NULL)){
      printf("Cant get gateway name\n");
      strcpy(vGates[i]->type,"err");
      continue;
    }else{
       printf("gateway:%d type:%s\n",i,tmp);
      strcpy(vGates[i]->type,tmp);
    }
    sprintf(command,"gateway%d.port",i);
    ret=get(command,tmp);
    if((ret)||(tmp==NULL)){
      printf("Cant get gateway port\n");
      strcpy(vGates[i]->type,"err");//to avoid select problems
    }else{
//       printf("gateway:%d type:%s==ip[%s]\n",i,vGates[i]->type,tmp);
      if(strcmp("ip",vGates[i]->type)==0){
	vGates[i]->fd=openUdpSocket(atoi(tmp));
      }else if(strcmp("err",vGates[i]->type)!=0){
	vGates[i]->fd=openSerial(tmp);
      }
      printf("INFO:Gatway%d[%s] with fd[%d]\n",i,vGates[i]->type,vGates[i]->fd);
    }
  }//for
}//func

int readModules(void){
  printf("readModules\n");
  char tmp[KEY_LEN];
  char command[KEY_LEN];
  if(get("global.nmodules",tmp)){
    perror("reading config problems:\n");
    return 1;
  }
  if(tmp==NULL){
    perror("reading global.nmodules:\n");
    return 1;
  }
  nModules=atoi(tmp);
 
  int i,j,k,bBreak=0,ret;
  vModules=(sModule**) malloc (nModules*sizeof(char*));
  for( i=0;i<nModules;i++){
    vModules[i]=(sModule *) malloc (sizeof(sModule));
    for(j=0;j<NUM_OPT;j++){
      printf("mod:%d,opt:%d\n",i,j);
      if(bBreak){//fallo lectura del modulo seguimos con el siguiente
	bBreak=0;
	break;
      }
      sprintf(command,"module%d.%s",i,opt[j]);
      ret=get(command,tmp);
 
      switch(j){
	
	case 0:{
	  if(ret||(tmp==NULL)){	//debe tener id
	    fprintf(stderr,"reading config problems:module%d.%s\n",i,opt[j]);
	    bBreak=1;
	    break;
	  }
	  strcpy(vModules[i]->id,tmp);
	}
	break;
	case 1:{
	   if(ret||(tmp==NULL)){		//debe tener gateway
	    fprintf(stderr,"reading config problems:module%d.%s\n",i,opt[j]);
	    bBreak=1;
	    break;
	  }
	  strcpy(vModules[i]->gate,tmp);
	}
	break;
	case 2:{
	   if(ret||(tmp==NULL)){		//debe tener nombre
	    fprintf(stderr,"reading config problems:module%d.%s\n",i,opt[j]);
	    bBreak=1;
	    break;
	  }
	  strcpy(vModules[i]->ip,tmp);
	}
	break;
	case 3:{
	  if((!ret)&&(tmp!=NULL))
	    strcpy(vModules[i]->name,tmp);
	}
	break;
	case 4:{
	   if((!ret)&&(tmp!=NULL))
	    strcpy(vModules[i]->feed,tmp);
	}
	break;
	case 5:{
	    if((!ret)&&(tmp!=NULL))
	    strcpy(vModules[i]->key,tmp);
	}
	break;
	case 6:{
	  if(ret){
	    printf("mod:%d, not params\n",i);
	    strcpy(vModules[i]->nparam,"0");
	    break;
	  }
	  int n=atoi(tmp);
	  strcpy(vModules[i]->nparam,tmp);
// 	  vModules[i]->nparam=n;
	  vModules[i]->params=(char**) malloc (n*sizeof(char*));
	  for( k=0;k<n;k++){
	    printf("n:%d,module%d.param%d\n",n,i,k);
	    sprintf(command,"module%d.param%d",i,k);
	    ret=get(command,tmp);
	    if(ret||(tmp==NULL)){		//debe tener nombre
	      fprintf(stderr,"reading config problems:module%d.param%d\n",i,k);
	      strcpy(vModules[i]->params[k],"0");
	      break;
	    }
	    vModules[i]->params[k]=(char *) malloc (strlen(tmp)+1);
	    printf("mod:%d,param:%d,[%s]\n",i,k,tmp);
	    strcpy(vModules[i]->params[k],tmp);
	  }
	}
	break;
	
	default:
	  perror("problem with num of options");
	  break;
      }//switch
    }//for j
  }//for i
  return 0;
}//readfile

void cli_perror(void){
  if(flags & CLI_FLAG_QUIET)
    return;
  uci_perror(ctx, appname);
};


