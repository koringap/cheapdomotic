#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <fcntl.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/types.h> 
 
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>

#define PAGE "/"
#define PORT 80
#define USERAGENT "HTMLGET 1.0"
#define TAM_SOCKET sizeof(struct sockaddr_in) 
#define IP_MAX 16
#define HTTP_PORT 80
#define HTTP  "http://"
#define MSG_MAX 100 //TODO: look at it
#define STR_LEN 200
#define KEY_LEN 200 //TODO: look at it
#define HOST "api.pachube.com"


char g_msg[MSG_MAX]; //for serial and udp process internals msg
char g_buf_serial[MSG_MAX]; //for serial and udp process internals msg
int  g_buf_pos,g_buf_new;

struct sockaddr_in  g_sockadd_data;     
struct sockaddr_in  g_sockadd_conf;

int createTcpSocket();
int connectTcpSocket(int sock,char * ip);
int resolveHost(char *host,char *ip);
int sendTcpData(char * data, char * host,struct sockaddr_in * sockadd,int hostip);
int senUdpdData( int fd,char *buffer, unsigned int len);
int connectServer(void);
int openUdpSocket(int puerto );
int initPachube(void);
char *buildPachubeQuery(char *feed, char *key, char * data);
void disconnectServer(int fd);
int openSerial(char *);
int readSerial(int s);
int readSocket(int s);
int printDS(struct sockaddr_in ds);