#ejemplo de usando CMake para cross
INCLUDE(CMakeForceCompiler)
cmake_minimum_required(VERSION 2.6)
# cmake_policy(SET CMP0011 OLD)
SET(CMAKE_CXX_COMPILER mipsel-openwrt-linux-uclibc-g++)
#MESSAGE(STATUS "babosos")
#mipsel-openwrt-linux-uclibc-ld
# If your target is an embedded system without OS set CMAKE_SYSTEM_NAME to "Generic". 
# If CMAKE_SYSTEM_NAME is preset, the CMake variable CMAKE_CROSSCOMPILING is automatically set to TRUE, 
# so this can be used for testing in the CMake files. 
SET (CMAKE_SYSTEM_NAME Generic )
SET (CMAKE_CROSSCOMPILING true)

SET (CMAKE_SYSTEM_VERSION 1)
SET (CMAKE_SYSTEM_PROCESSOR arm)


# specify the cross compiler
SET(CMAKE_C_COMPILER   mipsel-openwrt-linux-uclibc-gcc)
SET(CMAKE_CXX_COMPILER mipsel-openwrt-linux-uclibc-g++)
SET(CMAKE_AR mipsel-openwrt-linux-uclibc-ar)
SET(CMAKE_RANLIB mipsel-openwrt-linux-uclibc-ranlib)


# where is the target environment 
# SET(CMAKE_FIND_ROOT_PATH  ake_cc )
#SET(CMAKE_FIND_ROOT_PATH  /opt/memoria/OpenWRT/trunk/staging_dir/toolchain-mipsel_r2_gcc-4.6-linaro_uClibc-0.9.33.2/bin )

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

