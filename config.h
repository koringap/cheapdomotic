
#define KEY_LEN 200 
#define NUM_OPT 7

typedef struct module{
  char id[5];
  char gate[10];
  char name[10];
  char feed[10];
  char ip[15];
  char key[KEY_LEN];
  char ** params;
  char  nparam[3];//hasta 99 parametros
}sModule;

typedef struct gate{
  char type[10];
  int 	fd;
}sGate;

// char * opt[];

int nModules,nGates;
sModule ** vModules;
sGate	** vGates;
void initConfig(char *confFile);

void cli_perror(void);

int get(char *data, char *dst);

int readConfig(void);

void configModules(void);


