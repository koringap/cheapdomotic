#include "tools.h"
#include "config.h"

#define TIMEOUT     1000          /* number of seconds to wait */

// int  fds[2];             /* file descriptors          */
// struct sockaddr_in sock; 
// int s, n; 
// socklen_t len; 

int processData(void);
int sendSensorData(unsigned int feed,char * api_key,int data,int len );
int sensorMsg(int length,int module,int position);

void readFd(fd_set *);

int g_sock;
int g_ngetway;
// int fd = 0;
int main(int argc, char **argv){
  fd_set readfds;
  int i, n, maxfd;
  struct timeval tv;
  char res[10];
  g_buf_pos=0,g_buf_new=1;
  printf("init\n");
  initConfig("domotica");
  readConfig();
  configModules();
  printf("INFO: End config\n");
  
  /*
  * Forever...
  */
  printf("INFO: Enter in loop\n");
  for (;;) {
    /*
    * Zero the bitmask.
    */
    FD_ZERO(&readfds);

    /*
    * Set bits in the bitmask.
    */
    FD_SET(vGates[0]->fd, &readfds);
    for(i=1;i<nGates;i++){
      FD_SET(vGates[i]->fd, &readfds);
      printf("INFO:Gate set [%s]\n",vGates[i]->type);
      
    }

    /*
    * Set up the timeout.
    */
    tv.tv_sec = TIMEOUT;
    tv.tv_usec = 0;

    /*
    * Wait for some input.
    */
    printf("\nWaiting on select:.\n");
    n = select(maxfd, &readfds, (fd_set *) 0, (fd_set *) 0, &tv);
//     printf("\nselect:%d.\n",n);
    /*
    * See what happened.
    */
  switch (n) {
    case -1:            /* error           */
    perror("select");
//       exit(1);
      break;
    case 0:             /* timeout         */
      printf("\nTimeout expired.\n");
      break;
    default:            /* input available */
      readFd(&readfds);
      break;
    }
  }
}

void readFd(fd_set *readfds){
  printf("readFd");
  int i,ret1=1,ret2=1;
  if (FD_ISSET(vGates[0]->fd, readfds)) {
    if(readUdpSocket(vGates[0]->fd)==0)
      processData();
  }
  for(i=1;i<nGates;i++){
     printf("asd1:%d",FD_ISSET(vGates[i]->fd, readfds));
    if (FD_ISSET(vGates[i]->fd, readfds)) {
//       printf("asd1:%d",i);
      if(readSerial(vGates[i]->fd)==0){
// 	printf("asd");
	processData();
      }
    }
  }
}

int processData(void){
  printf("processData:\n");
  int i,j,k,nWord=-1,idModule=-1,datWorda=0,msgType=-1,msgLength=-1;
  char tmp[20];
  for(i=0,k=0;(g_msg[i]!='\n')&&(g_msg[i]!='\0');i++,k++){
//     printf("{%c}\n",g_msg[i]);
    if(g_msg[i]!=' '){
      tmp[k]=g_msg[i];
      continue;
    }else{
      tmp[k]='\0';
      nWord++;
//       printf("word%d:[%s],[%d]\n",nWord,tmp,k);
    }
    switch(nWord){
      case 0:{		//1° word is the module name
	printf("DBG, proces id\n");
	for(j=0;j<nModules;j++){
// 	  printf("DBG, compare %s=%s \n",tmp,vModules[j]->id);
	  if(strcmp(tmp,vModules[j]->id) == 0)
	    printf("DBG, proces id %d\n",j);
	    idModule=j;
	    break;
	}
	if(idModule==-1){
	  printf("ERR, proces id fail\n");
	  return 1;
	}
      }
      break;
      case 1:{          //2° word is msg types 0:sensor data, 1:getway resend;
	if((strlen(tmp)!=1)||(tmp==NULL)){
	  printf("ERR, proces type fail[%p],[%d],[%s]\n",tmp,(int)strlen(tmp),tmp);
	  return 1;
	}
	msgType=atoi(tmp);
	printf("DBG, msg Type is: %d\n",msgType);
      }
      break;
      case 2:{		//3° word msg payload length (in words)
	if((strlen(tmp)!=1)||(tmp==NULL)){
	  printf("ERR, proces type fail\n");
	  return 1;
	}
	msgLength=atoi(tmp);
	printf("DBG, msg Length is: %d\n",msgLength);
	switch(msgType){
	  case 0:
	    return sensorMsg(msgLength,idModule,i);
// 	  case 1:
// 	    return GatewayMsg();
	  default:
	    printf("ERR, msg type no mapped\n");
	    return 1;
	}
      }
      break;
      default:{
	printf("ERR, nword no mapped\n");
      }
      break;
      
    }//switsh
    memset(tmp,0,20);
    k=-1;
  }//for
  memset(tmp,0,20);
  return 1;
}

int sensorMsg(int w,int module,int pos){
  printf("sensorMsg:\n");
  char tmp[20];
  int i,k,count=0;
  char *pmsg;
  for(i=pos,k=0;(g_msg[i]!='\n')&&(g_msg[i]!='\0');i++,k++){
//     printf("{%c}\n",g_msg[i]);
    if(g_msg[i]!=' '){
      tmp[k]=g_msg[i];
      continue;
    }else{
      count++;
      tmp[k]='\n';
    }
  }
//   printf("words:[%s],[%d]\n",tmp,count);
  if(count <w-1){
    printf("ERR, not enought words in msg payload\n");
    return 1;
  }
  
//   char ip[16];
  pmsg=buildPachubeQuery(vModules[module]->feed, vModules[module]->key, tmp);
//   int sock;
//   =createTcpSocket();
//   if(resolveHost("api.cosm.com",ip))
 
//   ip=resolveHost("api.pachube.com",ip);
//   if(sock!=-1)
//   if(connectTcpSocket(sock,ip))
//     return 1;
  printf("sendMSG[%s]\n",pmsg);
  return sendTcpData(pmsg,"api.pachube.com",&g_sockadd_data,1);
}



void configModules(void){
  printf("configModules\n");
  int i,j,k;
  char msg[MSG_MAX];
  for(i=0;i<nModules;i++){
    int n=atoi(vModules[i]->nparam);
    printf("mod:%d,n:%d\n",i,n);
    if(n){
      printf("%s!=ip j=0\n",vModules[i]->gate);
      if(strcmp("ip",vModules[i]->gate)==0){//ip is default gate in pos=0;
	printf("ip, mod:%d,[%s]",i,vModules[i]->id);
	memset(msg,0,MSG_MAX);
	strcat(msg,vModules[i]->id);
	strcat(msg," 2 ");
	strcat(msg,vModules[i]->nparam);
	for(k=0;(k<n);k++){
	  strcat(msg," ");
	  strcat(msg,vModules[i]->params[k]);
	}
	strcat(msg,"\n");
	printf("[%s]\n",msg);
	sendTcpData(msg,vModules[i]->ip,&g_sockadd_conf,0);
      }else{
	printf("nGates:%d\n",nGates);
	int found=0;
	for(j=1;j<nGates;j++){ 
	  printf("%s!=%s in j:%d\n",vModules[i]->gate,vGates[j]->type,j);
	  if(strcmp(vModules[i]->gate,vGates[j]->type)==0){
	    found=1;
	    memset(msg,0,MSG_MAX);
	    strcat(msg,vModules[i]->id);
	    strcat(msg," 2 ");
	    strcat(msg,vModules[i]->nparam);
	    for(k=0;(k<n);k++){
	      strcat(msg," ");
	      strcat(msg,vModules[i]->params[k]);
	    }
	    strcat(msg,"\n");
	    printf("[%s]\n",msg);
	    write(vGates[j]->fd,msg,strlen(msg));
	  }
	}//for nGate
	printf("found:%d\n",found);
      }//type of gate
    }else{
      printf("module%d: without params\n ",i);
    }
  }//for modules
}//function

